package com.marketplace.mycatalog.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import javax.validation.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity @Data
public class ProductPhotos {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_product", nullable = false)
    private Product product;

    @NotNull @NotEmpty
    private String url;
}
