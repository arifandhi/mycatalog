package com.marketplace.mycatalog.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity @Data
public class Product {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    // Product(String id) {
    //     this.id=id;
    // }
    public String getId() {
        return this.id;
    }

    // public void setId(String id) {
    //     this.id = id;
    // }

    @NotNull @NotEmpty @Size(max = 100)
    private String code;

    @NotNull @NotEmpty
    private String name;

    @NotNull @Min(1)
    private BigDecimal weight;

    @NotNull @Min(1000)
    private BigDecimal price;
}
