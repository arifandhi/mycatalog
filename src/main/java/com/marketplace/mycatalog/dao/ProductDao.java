package com.marketplace.mycatalog.dao;

import java.util.List;

import com.marketplace.mycatalog.entity.Product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductDao extends JpaRepository<Product, String>{
    // public Product findById(Product p); 
    // Product findById(Product id); 
    // Product findByUuid(String id);
    // Product findOne(String primaryKey);
}