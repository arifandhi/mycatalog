package com.marketplace.mycatalog.dao;

import com.marketplace.mycatalog.entity.Product;
import com.marketplace.mycatalog.entity.ProductPhotos;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductPhotosDao extends PagingAndSortingRepository<ProductPhotos, String>{
    public Iterable<ProductPhotos> findByProduct(Product product);
}