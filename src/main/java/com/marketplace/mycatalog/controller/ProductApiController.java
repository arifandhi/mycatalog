package com.marketplace.mycatalog.controller;

import java.util.NoSuchElementException;

import com.marketplace.mycatalog.dao.ProductDao;
import com.marketplace.mycatalog.dao.ProductPhotosDao;
import com.marketplace.mycatalog.entity.Product;
import com.marketplace.mycatalog.entity.ProductPhotos;
import com.marketplace.mycatalog.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/product/")
public class ProductApiController {
    // private static final Logger logger = LoggerFactory.getLogger(ProductApiController.class);
    @Autowired
    private ProductDao productDao;
    @Autowired
    private ProductService productService;
    @Autowired private ProductPhotosDao productPhotosDao;

   
    @GetMapping("/")
    public Page<Product> findProducts(Pageable page) {
        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> get(@PathVariable String id) {
        try {
            Product product = productService.get(id);
            return new ResponseEntity<Product>(product, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/")
    public void add(@RequestBody Product product) {
        productService.save(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Product product, @PathVariable String id) {
        try {
            // Product existProduct = productService.get(id);
            productService.save(product);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        productService.delete(id);
    }

    @GetMapping("/{id}/photos")
    public Iterable<ProductPhotos> findPhotosForProduct(@PathVariable("id") Product p){
        return productPhotosDao.findByProduct(p);
    }
}