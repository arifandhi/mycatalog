package com.marketplace.mycatalog.service;

import java.util.List;

// import javax.transaction.Transactional;

import com.marketplace.mycatalog.dao.ProductDao;
import com.marketplace.mycatalog.entity.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
// @Transactional
public class ProductService {
    @Autowired
    private ProductDao repo;
     
    public List<Product> listAll() {
        return repo.findAll();
    }
     
    public void save(Product product) {
        repo.save(product);
    }
     
    public Product get(String id) {
        return repo.findById(id).get();
    }
     
    public void delete(String id) {
        repo.deleteById(id);
    }
}